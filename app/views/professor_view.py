from flask import Flask, Blueprint, render_template, request, redirect, url_for
from app.services.professor_services import *
from json import dumps

bp_professor = Blueprint('professor_blueprint', __name__)

@bp_professor.route('/hire', methods=['GET'])
def hire_form():
    return render_template('hire_form.html')


@bp_professor.route('/hire', methods=['POST'])
def hire():
    hire_professor(request.form['name'], request.form['coursename'])
    
    return redirect(url_for('professor_blueprint.hire'))


@bp_professor.route('/professor', methods=['GET'])
def list_professor():
    listed = list_all_professors('professors.csv')
    
    return render_template('list_professor.html', listed=listed)