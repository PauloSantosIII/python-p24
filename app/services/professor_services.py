import csv
from app.models.professor_models import Professor
from app.models.wizard_models import Wizard
from app.views.professor_view import *

filename = 'professors.csv'

def hire_professor(name, coursename):
    hired_professor = Professor(name, coursename)

    id = 0
    with open(filename, 'r') as id_csv:
        reader = csv.reader(id_csv)
        for line in reader:
            id += int(1)

    with open(filename, 'a') as f:
        writer = csv.writer(f, delimiter=',', lineterminator='\n')
        new_line = [id, hired_professor.name, hired_professor.coursename]
        writer.writerow(new_line)

    professors_list = zip(new_line)

    return dict(professors_list)


def list_all_professors(filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        professor_list = reader

    return professor_list


list_all_professors(filename)