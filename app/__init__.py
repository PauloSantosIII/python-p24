from flask import Flask
from app.views.professor_view import bp_professor


def create_app():
    app = Flask(__name__)
    app.register_blueprint(bp_professor)

    return app